// ***********************************************************************
//
// Demo program for education in subject
// Computer Architectures and Parallel Systems.
// Petr Olivka, dep. of Computer Science, FEI, VSB-TU Ostrava
// email:petr.olivka@vsb.cz
//
// Example of CUDA Technology Usage wit unified memory.
// Image transformation from RGB to BW schema. 
//
// ***********************************************************************

#include <stdio.h>
#include <cuda_device_runtime_api.h>
#include <cuda_runtime.h>
#include <stdint.h>
#include "pic_type.h"

__global__ void kernel_cut_ins(CUDA_Pic bigPic, CUDA_Pic smallPic, int2 position, int ins_or_cut)
{
    // X,Y coordinates and check image dimensions
    int y = blockDim.y * blockIdx.y + threadIdx.y;
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    if ( y >= smallPic.Size.y ) return;
    if ( x >= smallPic.Size.x ) return;
    int by = y + position.y;
    int bx = x + position.x;
    if ( by >= bigPic.Size.y || by < 0 ) return;
    if ( bx >= bigPic.Size.x || bx < 0 ) return;

    if(ins_or_cut == 1)
    {
        bigPic.at<uchar3>(by, bx) = smallPic.at<uchar3>(y, x);
    } else {
        smallPic.at<uchar3>(y, x) = bigPic.at<uchar3>(by, bx);
    }
}

void cu_ins_cut(CUDA_Pic t_bigpic, CUDA_Pic t_smallpic, int2 t_position, int t_ins_or_cut)
{
    cudaError_t cerr;

    // Grid creation, size of grid must be equal or greater than images
    int block_size = 32;
    dim3 blocks( ( t_bigpic.Size.x + block_size - 1 ) / block_size, ( t_bigpic.Size.y + block_size - 1 ) / block_size );
    dim3 threads( block_size, block_size );
    kernel_cut_ins<<< blocks, threads >>>( t_bigpic, t_smallpic, t_position, t_ins_or_cut );

    if ( ( cerr = cudaGetLastError() ) != cudaSuccess )
        printf( "CUDA Error [%d] - '%s'\n", __LINE__, cudaGetErrorString( cerr ) );

    cudaDeviceSynchronize();
}

__global__ void kernel_rotate(CUDA_Pic pic, CUDA_Pic out,float angle)
{
    int y = blockDim.y * blockIdx.y + threadIdx.y;
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    if ( y >= pic.Size.y ) return;
    if ( x >= pic.Size.x ) return;

     out.at<uchar3>(x, y) = pic.at<uchar3>(y, x);
}

void cu_rotate(CUDA_Pic t_pic, CUDA_Pic t_out, float t_angle)
{
    cudaError_t cerr;

    // Grid creation, size of grid must be equal or greater than images
    int block_size = 32;
    dim3 blocks( ( t_pic.Size.x + block_size - 1 ) / block_size, ( t_pic.Size.y + block_size - 1 ) / block_size );
    dim3 threads( block_size, block_size );
    kernel_rotate<<< blocks, threads >>>( t_pic, t_out, t_angle );

    if ( ( cerr = cudaGetLastError() ) != cudaSuccess )
        printf( "CUDA Error [%d] - '%s'\n", __LINE__, cudaGetErrorString( cerr ) );

    cudaDeviceSynchronize();
}

__global__ void kernel_select_color(CUDA_Pic inp, CUDA_Pic outp, int layer)
{
    int y = blockDim.y * blockIdx.y + threadIdx.y;
    int x = blockDim.x * blockIdx.x + threadIdx.x;
    if ( y >= inp.Size.y ) return;
    if ( x >= inp.Size.x ) return;


    if(layer == 1)
    {
        outp.at<uchar3>(y, x) = { inp.at<uchar3>(y, x).x , 0, 0};
    } else if(layer == 2) {
        outp.at<uchar3>(y, x) = { 0, inp.at<uchar3>(y, x).y, 0};
    } else {
        outp.at<uchar3>(y, x) = { 0, 0, inp.at<uchar3>(y, x).z };
    }
}

void cu_select_color(CUDA_Pic t_inp, CUDA_Pic t_outp, int t_layer)
{
    cudaError_t cerr;

    // Grid creation, size of grid must be equal or greater than images
    int block_size = 32;
    dim3 blocks( ( t_inp.Size.x + block_size - 1 ) / block_size, ( t_inp.Size.y + block_size - 1 ) / block_size );
    dim3 threads( block_size, block_size );
    kernel_select_color<<< blocks, threads >>>( t_inp, t_outp, t_layer );

    if ( ( cerr = cudaGetLastError() ) != cudaSuccess )
        printf( "CUDA Error [%d] - '%s'\n", __LINE__, cudaGetErrorString( cerr ) );

    cudaDeviceSynchronize();
}

// Demo kernel to create chess board
__global__ void kernel_chessboard( CUDA_Pic colorPic )
{
	// X,Y coordinates and check image dimensions
	int y = blockDim.y * blockIdx.y + threadIdx.y;
	int x = blockDim.x * blockIdx.x + threadIdx.x;
	if ( y >= colorPic.Size.y ) return;
	if ( x >= colorPic.Size.x ) return;

	unsigned char borw = 255 * ( ( blockIdx.x + blockIdx.y ) & 1 );

	// Store point into image
//	colorPic.P_uchar3[ y * colorPic.Size.x + x ] = { borw, borw, borw };
//	colorPic.at3((uint32_t) y, (uint32_t)x) = { borw, borw, borw };
	colorPic.at<uchar3>((uint32_t) y, (uint32_t)x) = { borw, borw, borw };
}

void cu_create_chessboard( CUDA_Pic colorPic, int square_size )
{
	cudaError_t cerr;

	// Grid creation, size of grid must be equal or greater than images
	dim3 blocks( ( colorPic.Size.x + square_size - 1 ) / square_size, ( colorPic.Size.y + square_size - 1 ) / square_size );
	dim3 threads( square_size, square_size );
	kernel_chessboard<<< blocks, threads >>>( colorPic );

	if ( ( cerr = cudaGetLastError() ) != cudaSuccess )
		printf( "CUDA Error [%d] - '%s'\n", __LINE__, cudaGetErrorString( cerr ) );

	cudaDeviceSynchronize();
}

// -----------------------------------------------------------------------------------------------

// Demo kernel to create picture with alpha channel gradient
__global__ void kernel_alphaimg( CUDA_Pic colorPic, uchar3 color )
{
	// X,Y coordinates and check image dimensions
	int y = blockDim.y * blockIdx.y + threadIdx.y;
	int x = blockDim.x * blockIdx.x + threadIdx.x;
	if ( y >= colorPic.Size.y ) return;
	if ( x >= colorPic.Size.x ) return;

	int diagonal = sqrtf( colorPic.Size.x * colorPic.Size.x + colorPic.Size.y * colorPic.Size.y );
	int dx = x - colorPic.Size.x / 2;
	int dy = y - colorPic.Size.y / 2;
	int dxy = sqrtf( dx * dx + dy * dy ) - diagonal / 2;

	// Store point into image
    colorPic.P_uchar4[ y * colorPic.Size.x + x ] =
		{ color.x, color.y, color.z, ( unsigned char ) ( 255 - 255 * dxy / ( diagonal / 2 ) ) };

    colorPic.at4((uint32_t) y, (uint32_t)x) =
            { color.x, color.y, color.z, ( unsigned char ) ( 255 - 255 * dxy / ( diagonal / 2 ) ) };
}

void cu_create_alphaimg( CUDA_Pic colorPic, uchar3 color )
{
	cudaError_t cerr;

	// Grid creation, size of grid must be equal or greater than images
	int block_size = 32;
	dim3 blocks( ( colorPic.Size.x + block_size - 1 ) / block_size, ( colorPic.Size.y + block_size - 1 ) / block_size );
	dim3 threads( block_size, block_size );
	kernel_alphaimg<<< blocks, threads >>>( colorPic, color );

	if ( ( cerr = cudaGetLastError() ) != cudaSuccess )
		printf( "CUDA Error [%d] - '%s'\n", __LINE__, cudaGetErrorString( cerr ) );

	cudaDeviceSynchronize();
}

// -----------------------------------------------------------------------------------------------

// Demo kernel to create picture with alpha channel gradient
__global__ void kernel_insertimage( CUDA_Pic bigPic, CUDA_Pic smallPic, int2 position )
{
	// X,Y coordinates and check image dimensions
	int y = blockDim.y * blockIdx.y + threadIdx.y;
	int x = blockDim.x * blockIdx.x + threadIdx.x;
	if ( y >= smallPic.Size.y ) return;
	if ( x >= smallPic.Size.x ) return;
	int by = y + position.y;
	int bx = x + position.x;
	if ( by >= bigPic.Size.y || by < 0 ) return;
	if ( bx >= bigPic.Size.x || bx < 0 ) return;

	// Get point from small image
	uchar4 fg_bgra = smallPic.P_uchar4[ y * smallPic.Size.x + x ];
	uchar3 bg_bgr = bigPic.P_uchar3[ by * bigPic.Size.x + bx ];
	uchar3 bgr = { 0, 0, 0 };

	// compose point from small and big image according alpha channel
	bgr.x = fg_bgra.x * fg_bgra.w / 255 + bg_bgr.x * ( 255 - fg_bgra.w ) / 255;
	bgr.y = fg_bgra.y * fg_bgra.w / 255 + bg_bgr.y * ( 255 - fg_bgra.w ) / 255;
	bgr.z = fg_bgra.z * fg_bgra.w / 255 + bg_bgr.z * ( 255 - fg_bgra.w ) / 255;

	// Store point into image
	bigPic.P_uchar3[ by * bigPic.Size.x + bx ] = bgr;
}

void cu_insertimage( CUDA_Pic bigPic, CUDA_Pic smallPic, int2 position )
{
	cudaError_t cerr;

	// Grid creation, size of grid must be equal or greater than images
	int block_size = 32;
	dim3 blocks( ( smallPic.Size.x + block_size - 1 ) / block_size, ( smallPic.Size.y + block_size - 1 ) / block_size );
	dim3 threads( block_size, block_size );
	kernel_insertimage<<< blocks, threads >>>( bigPic, smallPic, position );

	if ( ( cerr = cudaGetLastError() ) != cudaSuccess )
		printf( "CUDA Error [%d] - '%s'\n", __LINE__, cudaGetErrorString( cerr ) );

	cudaDeviceSynchronize();
}
