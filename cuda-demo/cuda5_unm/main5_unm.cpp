// ***********************************************************************
//
// Demo program for education in subject
// Computer Architectures and Parallel Systems.
// Petr Olivka, dep. of Computer Science, FEI, VSB-TU Ostrava
// email:petr.olivka@vsb.cz
//
// Example of CUDA Technology Usage with unified memory.
//
// Image transformation from RGB to BW schema. 
// Image manipulation is performed by OpenCV library. 
//
// ***********************************************************************

#include <stdio.h>
#include <cuda_device_runtime_api.h>
#include <cuda_runtime.h>
#include "opencv2/opencv.hpp"
#include "pic_type.h"
#include "uni_mem_allocator.h"

using namespace cv;

// Function prototype from .cu file
void cu_create_chessboard( CUDA_Pic pic, int square_size );
void cu_create_alphaimg( CUDA_Pic pic, uchar3 color );
void cu_insertimage( CUDA_Pic bigPic, CUDA_Pic smallPic, int2 position );
void cu_ins_cut(CUDA_Pic t_bigpic, CUDA_Pic t_smallpic, int2 t_position, int t_ins_or_cut);
void cu_rotate(CUDA_Pic t_pic, CUDA_Pic t_out, float t_angle);
void cu_select_color(CUDA_Pic t_inp, CUDA_Pic t_outp, int t_layer);

int main( int numarg, char **arg )
{
	// Uniform Memory allocator for Mat
	UniformAllocator allocator;
	cv::Mat::setDefaultAllocator( &allocator );

//	Mat cv_chessboard( 511, 515, CV_8UC3 );
//
//	CUDA_Pic pic_chessboard;
//	pic_chessboard.Size.x = cv_chessboard.cols;
//	pic_chessboard.Size.y = cv_chessboard.rows;
//	pic_chessboard.P_uchar3 = ( uchar3 * ) cv_chessboard.data;
//
//	CUDA_Pic pic_chessboard2(&cv_chessboard);
//
//	cu_create_chessboard( pic_chessboard2, 21 );
//
////	imshow( "Chess Board", cv_chessboard );
//
//	Mat cv_alphaimg( 211, 191, CV_8UC4 );
//
//	CUDA_Pic pic_alphaimg;
//	pic_alphaimg.Size.x = cv_alphaimg.cols;
//	pic_alphaimg.Size.y = cv_alphaimg.rows;
//	pic_alphaimg.P_uchar4 = ( uchar4 * ) cv_alphaimg.data;
//
//	cu_create_alphaimg( pic_alphaimg, { 0, 0, 255 } );
//
////	imshow( "Alpha channel", cv_alphaimg );
//
//	cu_insertimage( pic_chessboard, pic_alphaimg, { 11, 23 } );
//
//	imshow( "Result I", cv_chessboard );

        const char *path = "/home/michal/Obrázky/wallpaper.jpg";
		// Load image
		Mat cv_ball = imread( path, CV_LOAD_IMAGE_UNCHANGED );
        Mat cv_small( 250, 250, CV_8UC3 );
        Mat cv_small_rot( 250, 250, CV_8UC3 );
        Mat cv_small_r( 250, 250, CV_8UC3 );
        Mat cv_small_g( 250, 250, CV_8UC3 );
        Mat cv_small_b( 250, 250, CV_8UC3 );

		if ( !cv_ball.data )
			printf( "Unable to read file '%s'\n", path );

//		else if ( cv_ball.channels() != 4 )
//			printf( "Image does not contain alpha channel!\n" );

		else
		{
			// insert loaded image
			CUDA_Pic pic_ball(&cv_ball);
			CUDA_Pic pic_small(&cv_small);
			CUDA_Pic pic_small_rot(&cv_small_rot);
			CUDA_Pic pic_small_r(&cv_small_r);
			CUDA_Pic pic_small_g(&cv_small_g);
			CUDA_Pic pic_small_b(&cv_small_b);

			cu_ins_cut(pic_ball, pic_small, {150, 150}, 0);

            cu_rotate(pic_small, pic_small_rot, 90.0);
            cu_select_color(pic_small_rot, pic_small_r, 3);
            cu_select_color(pic_small_rot, pic_small_g, 2);
            cu_select_color(pic_small_rot, pic_small_b, 1);
//            cu_ins_cut(pic_ball, pic_small, {150, 150}, 1);

			imshow( "Result I", cv_ball );
			imshow( "Result II", cv_small_rot );
//			imshow( "Result III", cv_small_r );
//			imshow( "Result IIII", cv_small_g );
//			imshow( "Result V", cv_small_b );
		}

	waitKey( 0 );
}

