// ***********************************************************************
//
// Demo program for education in subject
// Computer Architectures and Parallel Systems.
// Petr Olivka, dep. of Computer Science, FEI, VSB-TU Ostrava
// email:petr.olivka@vsb.cz
//
// Example of CUDA Technology Usage.
//
// Image interface for CUDA
//
// ***********************************************************************

#pragma once

#include <stdint.h>
#include "opencv2/opencv.hpp"

// Structure definition for exchanging data between Host and Device
struct CUDA_Pic {
    uint3 Size;            // size of picture
    union {
        void *P_void;        // data of picture
        uchar1 *P_uchar1;        // data of picture
        uchar3 *P_uchar3;        // data of picture
        uchar4 *P_uchar4;        // data of picture
    };

    CUDA_Pic() {}

    CUDA_Pic(cv::Mat *mat) {
        Size.x = mat->cols;
        Size.y = mat->rows;
        P_void = (void *) mat->data;
    }

    __host__ __device__ uchar1 &at1(uint32_t y, uint32_t x)
    {
        return ((uchar1*) P_uchar1)[Size.x * y + x];
    }

    __host__ __device__ uchar3 &at3(uint32_t y, uint32_t x)
    {
        return ((uchar3*) P_uchar3)[Size.x * y + x];
    }

    __host__ __device__ uchar4 &at4(uint32_t y, uint32_t x)
    {
        return ((uchar4*) P_uchar4)[Size.x * y + x];
    }

    template<typename T>
    __host__ __device__  T &at(uint32_t y, uint32_t x) {
        return ((T*) P_void)[Size.x * y + x];
    }
};
