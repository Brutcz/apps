// ***********************************************************************
//
// Demo program for subject Computer Architectures and Paralel systems 
// Petr Olivka, Dept. of Computer Science, FEECS, VSB-TU Ostrava
// email:petr.olivka@vsb.cz
//
// Threads programming example for Linux (10/2016)
// For the propper testing is necessary to have at least 2 cores CPU
//
// ***********************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/param.h>
#include <pthread.h>
#include <algorithm>
#include <functional>
#include <array>
#include <iostream>

using namespace std;

#define TYPE float
#define LENGTH_LIMIT 50000
#define INTERVAL_MAX 1000
#define INTERVAL_MIN -1000
#define SORT_FROM_TOP false
#define THREADS 5

void print_arr(TYPE *arr, int len)
{
    for (int i = 0; i < len; i++) {
        cout << arr[i] << ", ";
    }
    cout << endl;
}

void copy_arr(TYPE *source, TYPE *dest, int start, int stop)
{
    for(int i = start; i < stop; i++)
    {
        source[i] = dest[i];
    }
}

bool comparator(TYPE *a, TYPE *b, bool from_low)
{
    return ((from_low)? (*a < *b) : (*a > *b));
}

void mergeArrays(TYPE *left, TYPE *right, int s1, int s2, TYPE *out, int out_s, bool from_low)
{
    int i = 0, j = 0, k = out_s;

    // Traverse both array
    while (i < s1 && j < s2)
    {
        // Check if current element of first
        // array is smaller than current element
        // of second array. If yes, store first
        // array element and increment first array
        // index. Otherwise do same with second array
        if (comparator(&left[i], &right[j], from_low))
            out[k++] = left[i++];
        else
            out[k++] = right[j++];
    }

    // Store remaining elements of first array
    while (i < s1)
        out[k++] = left[i++];

    // Store remaining elements of second array
    while (j < s2)
        out[k++] = right[j++];
}

// merge function for merging two parts
void merge(TYPE *data, int low, int mid, int high, bool from_low)
{
    // n1 is size of left part and n2 is size
    // of right part
    int n1 = mid - low, n2 = high - mid;

    TYPE* left = new TYPE[n1];
    TYPE* right = new TYPE[n2];


    // storing values in left part
    for (int i = 0; i < n1; i++)
        left[i] = data[i + low];

    // storing values in right part
    for (int i = 0; i < n2; i++)
        right[i] = data[i + mid];

    mergeArrays(left, right, n1, n2, data, low, from_low);

    delete left;
    delete right;
}

class task_part
{
public:
    int id;                 // user identification
    int from, length;       // data range
    TYPE *data;             // array
    bool from_low;

    task_part(){};

    task_part( int myid, int first, int num, TYPE *ptr, bool from_low ) :
            id( myid ), from( first ), length( num ), data( ptr ), from_low(from_low) {}

    void swap(TYPE *xp, TYPE *yp)
    {
        TYPE temp = *xp;
        *xp = *yp;
        *yp = temp;
    };

    void bubble_sort()
    {
        for(int i = this->from; i < this->length - 1; i++){
            for(int j = this->from; j < this->length - 1; j++){
                if(comparator(&this->data[j+1], &this->data[j], this->from_low)) swap(&this->data[j+1], &this->data[j]);
            }
        }
    }

    void selection_sort()
    {
        for (int i = this->from; i < this->length; i++) {
            int maxIndex = i;
            for (int j = i + 1; j < this->length; j++) {
                if (comparator(&this->data[j], &this->data[maxIndex], this->from_low)) maxIndex = j;
            }
            swap(&this->data[i], &this->data[maxIndex]);
        }
    };

    void insertion_sort()
    {
        for (int i = this->from; i < this->length; i++) {
            int j = i + 1;
            TYPE tmp = this->data[j];
            while (j > 0 && comparator(&tmp, &this->data[j-1], this->from_low)) {
                this->data[j] = this->data[j-1];
                j--;
            }
            this->data[j] = tmp;
        }
    };

    void sort_range()
    {
        bubble_sort();
//        selection_sort();
//        insertion_sort();
    };

    void generate_range(){
        srand(this->id * this->id); // seed by thread id
//        srand(time(NULL));

        // printf("Random numbers generetion started...");
        for (int i = this->from; i < this->length; i++)
        {
//            this->data[i] = rand() % (this->length * 10);
            int range = (INTERVAL_MAX - INTERVAL_MIN + 1) * 100;
            int min = (INTERVAL_MIN * 100);
            TYPE num = ( rand() % range + min );
            this->data[i] = num / 100.0;
            if (!(i % LENGTH_LIMIT))
            {
                printf(".");
                fflush(stdout);
            }
        }
    };
    void show(){
        cout << endl << "Thread id: " << this->id << endl;
        for(int i = this->from; i < this->length; i++){
//            printf("%d, ", this->data[i]);
            cout << this->data[i] << ", ";
        }
    };
};

// Thread will search the largest element in array 
// from element arg->from with length of arg->length.
// Result will be stored to arg->max.
void *my_thread( void *void_arg )
{
    task_part *ptr_task = ( task_part * ) void_arg;

//    printf( "Thread %d started from %d with length %d...\n", ptr_task->id, ptr_task->from, ptr_task->length );

//    ptr_task->generate_range();
//    printf("Generated %d numbers in thread %d\n", ptr_task->length - ptr_task->from, ptr_task->id);
    ptr_task->sort_range();
//    printf("Sorted %d numbers in thread %d\n", ptr_task->length - ptr_task->from, ptr_task->id);
//    ptr_task->show();

    return NULL;
}

// Time interval between two measurements
int timeval_to_ms( timeval *before, timeval *after )
{
    timeval res;
    timersub( after, before, &res );
    return 1000 * res.tv_sec + res.tv_usec / 1000;
}

void run_threads(TYPE *data, int length, int no_of_threads, timeval *start, timeval *end, bool do_merge, bool from_low) {
    pthread_t threads[no_of_threads];
    task_part tasks[no_of_threads];

    int length_per_thread = (length) / no_of_threads;

    for (int i = 0; i < no_of_threads; i++) {
        tasks[i] = task_part(i + 1, i * length_per_thread, (i + 1) * length_per_thread, data, from_low);
    }
    // Time recording before searching
    gettimeofday(start, NULL);

    // Threads starting
    for (int i = 0; i < no_of_threads; i++) {
        pthread_create(&threads[i], NULL, my_thread, &tasks[i]);
    }

    // Waiting for threads completion
    for (int i = 0; i < no_of_threads; i++) {
        pthread_join(threads[i], NULL);
    }

    if (do_merge)
    {
        for (int i = 1; i < no_of_threads; i++) {
            merge(data, 0, (i * length_per_thread), (i + 1) * length_per_thread, from_low);
        }
    }

    gettimeofday(end, NULL);
}

int main( int na, char **arg )
{
    // The number of elements must be used as program argument
    // if ( na != 2 ) 
    // { 
    //     printf( "Specify number of elements, at least %d.\n", LENGTH_LIMIT ); 
    //     return 0; 
    // }
    int my_length = LENGTH_LIMIT;//atoi( arg[ 1 ] );
    if ( my_length < LENGTH_LIMIT )
    {
        printf( "The number of elements must be at least %d.\n", LENGTH_LIMIT );
        return 0;
    }

    // array allocation
    TYPE *my_array = new TYPE [ my_length ];
    if ( !my_array )
    {
        printf( "Not enought memory for array!\n" );
        return 1;
    }



//    printf( "\nNumber sorting using %d threads...\n", THREADS );
//    pthread_t threads[THREADS];
//    task_part *tasks[THREADS];
//
//    timeval time_before, time_after;
//
//    int length_per_thread = (my_length)/THREADS;
//
//    for(int i = 0; i < THREADS; i++){
//        tasks[i] = task_part(i+1, i*length_per_thread, (i+1)*length_per_thread, my_array);
//    }
//    // Time recording before searching
//    gettimeofday( &time_before, NULL );
//
//    // Threads starting
//    for(int i = 0; i < THREADS; i++){
//        pthread_create(&threads[i], NULL, my_thread, &tasks[i]);
//    }
//
//    // Waiting for threads completion
//    for(int i = 0; i < THREADS; i++){
//        pthread_join( threads[i], NULL );
//    }
//
//
//    for (int i = 1; i < THREADS; i++) {
//        merge(my_array, 0, (i * length_per_thread), (i + 1) * length_per_thread);
//    }

//    cout << "result" << endl;
//    print_arr(my_array, my_length);
//
//    // Time recording after searching
//    gettimeofday( &time_after, NULL );
//
//    printf( "Done in: %d [ms]\n", timeval_to_ms( &time_before, &time_after ) );

//    printf( "\nNumber sorting using one thread...\n" );
//
//    gettimeofday( &time_before, NULL );

//    // Searching in single thread
//    task_part single( 333, 0, my_length, my_array );
//    single.sort_range();
//    single.show();

//    gettimeofday( &time_after, NULL );
//
//    printf( "The sort time: %d [ms]\n", timeval_to_ms( &time_before, &time_after ) );

    task_part *tasks[THREADS];

    timeval time_before, time_after;

    int testing_threads = 4;

    // TODO: generate array
    task_part single( 333, 0, my_length, my_array, true );
    single.generate_range();

    // TODO: copy that array
    TYPE *my_cp_array = new TYPE[my_length];
    copy_arr(my_array, my_cp_array, 0, my_length);

    // TODO: sort (2 threads) from low to high - timeit
    run_threads(my_array, my_length, testing_threads, &time_before, &time_after, true, false);
    printf( "The sort time from low to high using %d threads: %d [ms]\n", testing_threads, timeval_to_ms( &time_before, &time_after ) );
//    print_arr(my_array, my_length);

    // TODO: sort (2 threads) sorted from low to high - timeit
    run_threads(my_array, my_length, testing_threads, &time_before, &time_after, true, false);
    printf( "The sort time of sorted using %d threads: %d [ms]\n", testing_threads, timeval_to_ms( &time_before, &time_after ) );

    // TODO: sort (2 threads) from high to low - timeit
    run_threads(my_array, my_length, testing_threads, &time_before, &time_after, true, true);
    printf( "The sort time from high to low using %d threads: %d [ms]\n", testing_threads, timeval_to_ms( &time_before, &time_after ) );

    // TODO: sort in sigle thread
    gettimeofday( &time_before, NULL );
    task_part single_sort(111, 0, my_length, my_cp_array, false);
    single_sort.sort_range();
    gettimeofday( &time_after, NULL );
    printf( "The sort time from low to high using 1 thread: %d [ms]\n", timeval_to_ms( &time_before, &time_after ) );

    delete my_array;
    return 0;
}
