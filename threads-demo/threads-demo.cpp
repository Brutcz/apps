// ***********************************************************************
//
// Demo program for subject Computer Architectures and Paralel systems 
// Petr Olivka, Dept. of Computer Science, FEECS, VSB-TU Ostrava
// email:petr.olivka@vsb.cz
//
// Threads programming example for Linux (10/2016)
// For the propper testing is necessary to have at least 2 cores CPU
//
// ***********************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/param.h>
#include <pthread.h>
#include <algorithm>
#include <functional>
#include <array>
#include <iostream>

using namespace std;

#define TYPE int
#define LENGTH_LIMIT 20
#define THREADS 4

void print_arr(TYPE *arr, int len)
{
    for (int i = 0; i < len; i++) {
        cout << arr[i] << ", ";
    }
    cout << endl;
}

void mergeArrays(TYPE left[], TYPE right[], int s1, int s2, TYPE out[], int out_s)
{
    int i = 0, j = 0, k = out_s;

    // Traverse both array
    while (i < s1 && j < s2)
    {
        // Check if current element of first
        // array is smaller than current element
        // of second array. If yes, store first
        // array element and increment first array
        // index. Otherwise do same with second array
        if (left[i] < right[j])
            out[k++] = left[i++];
        else
            out[k++] = right[j++];
    }

    // Store remaining elements of first array
    while (i < s1)
        out[k++] = left[i++];

    // Store remaining elements of second array
    while (j < s2)
        out[k++] = right[j++];
}

// merge function for merging two parts
void merge(TYPE *data, int low, int mid, int high)
{
    int* left = new TYPE[mid - low + 1];
    int* right = new TYPE[high - mid];

    // n1 is size of left part and n2 is size
    // of right part
    int n1 = mid - low + 1, n2 = high - mid, i, j;

    // storing values in left part
    for (int i = 0; i < n1; i++)
        left[i] = data[i + low];

    // storing values in right part
    for (int i = 0; i < n2; i++)
        right[i] = data[i + mid + 1];

    cout << "left" << endl;
    print_arr(left, n1);
    cout << "right" << endl;
    print_arr(right, n2);

    mergeArrays(left, right, n1, n2, data, low);
    cout << "pre_res" << endl;
    print_arr(data, low);
}

class task_part
{
public:
    int id;                 // user identification
    int from, length;       // data range
    TYPE *data;             // array

    task_part(){};

    task_part( int myid, int first, int num, TYPE *ptr ) :
        id( myid ), from( first ), length( num ), data( ptr ) {}

    void swap(TYPE *xp, TYPE *yp)
    {
        TYPE temp = *xp;
        *xp = *yp;
        *yp = temp;
    };

    void bubble_sort()
    {
        int i, j;
        for (i = this->from; i < this->length - 1; i++)
            // Last i elements are already in place
            for (j = this->from; j < this->length - i - 1; j++)
                if (this->data[j] > this->data[j + 1])
                    swap(&this->data[j], &this->data[j + 1]);
    };

    void selection_sort()
    {
        for (int i = this->from; i < this->length - 1; i++) {
            int maxIndex = i;
            for (int j = i + 1; j < this->length; j++) {
                if (this->data[j] > this->data[maxIndex]) maxIndex = j;
            }
            swap(&this->data[i], &this->data[maxIndex]);
        }
    };

    void insertion_sort()
    {
        for (int i = this->from; i < this->length - 1; i++) {
            int j = i + 1;
            int tmp = this->data[j];
            while (j > 0 && tmp > this->data[j-1]) {
                this->data[j] = this->data[j-1];
                j--;
            }
            this->data[j] = tmp;
        }
    };

    // A function to implement bubble sort
    void sort_range()
    {
        bubble_sort();
//        selection_sort();
//        insertion_sort();
    };

    void generate_range(){
        srand(this->id); // seed by thread id

        // printf("Random numbers generetion started...");
        for (int i = this->from; i < this->length; i++)
        {
            this->data[i] = rand() % (this->length * 10);
            if (!(i % LENGTH_LIMIT))
            {
                printf(".");
                fflush(stdout);
            }
        }
    };
    void show(){
        for(int i = this->from; i < this->length; i++){
            printf("%d, ", this->data[i]);
        }
    };
};

// Thread will search the largest element in array 
// from element arg->from with length of arg->length.
// Result will be stored to arg->max.
void *my_thread( void *void_arg )
{
    task_part *ptr_task = ( task_part * ) void_arg;

    printf( "Thread %d started from %d with length %d...\n",
        ptr_task->id, ptr_task->from, ptr_task->length );

    ptr_task->generate_range();
//    printf("Generated %d numbers in thread %d\n", ptr_task->length - ptr_task->from, ptr_task->id);
    ptr_task->sort_range();
//    printf("Sorted %d numbers in thread %d\n", ptr_task->length - ptr_task->from, ptr_task->id);
    ptr_task->show();

    return NULL;
}

// Time interval between two measurements
int timeval_to_ms( timeval *before, timeval *after )
{
    timeval res;
    timersub( after, before, &res );
    return 1000 * res.tv_sec + res.tv_usec / 1000;
}

int main( int na, char **arg )
{
    // The number of elements must be used as program argument
    // if ( na != 2 ) 
    // { 
    //     printf( "Specify number of elements, at least %d.\n", LENGTH_LIMIT ); 
    //     return 0; 
    // }
    int my_length = LENGTH_LIMIT;//atoi( arg[ 1 ] );
    if ( my_length < LENGTH_LIMIT ) 
    { 
        printf( "The number of elements must be at least %d.\n", LENGTH_LIMIT ); 
        return 0; 
    }

    // array allocation
    TYPE *my_array = new TYPE [ my_length ];
    if ( !my_array ) 
    {
        printf( "Not enought memory for array!\n" );
        return 1;
    }

    

    printf( "\nNumber sorting using %d threads...\n", THREADS );
    pthread_t threads[THREADS];
    task_part tasks[THREADS];
    
    timeval time_before, time_after;

    int length_per_thread = (my_length)/THREADS;

    for(int i = 0; i < THREADS; i++){
        tasks[i] = task_part(i+1, i*length_per_thread, (i+1)*length_per_thread, my_array);
    }
    // Time recording before searching
    gettimeofday( &time_before, NULL );

    // Threads starting
    for(int i = 0; i < THREADS; i++){
        pthread_create(&threads[i], NULL, my_thread, &tasks[i]);
    }

    // Waiting for threads completion 
    for(int i = 0; i < THREADS; i++){
        pthread_join( threads[i], NULL );
    }

    cout << endl << "pre_pre_result" << endl;
    print_arr(my_array, 20);


    // merging the final 4 parts
    merge(my_array, 0, (my_length / 2 - 1) / 2, my_length / 2 - 1);
    merge(my_array, my_length / 2, my_length/2 + (my_length-1-my_length/2)/2, my_length - 1);
    merge(my_array, 0, (my_length - 1)/2, my_length - 1);

    cout << "result" << endl;
    print_arr(my_array, my_length);

    // Time recording after searching
    gettimeofday( &time_after, NULL );

    printf( "Done in: %d [ms]\n", timeval_to_ms( &time_before, &time_after ) );

//    printf( "\nNumber sorting using one thread...\n" );
//
//    gettimeofday( &time_before, NULL );

//    // Searching in single thread
//    task_part single( 333, 0, my_length, my_array );
//    single.sort_range();
//    single.show();

//    gettimeofday( &time_after, NULL );
//
//    printf( "The sort time: %d [ms]\n", timeval_to_ms( &time_before, &time_after ) );
}
