// **************************************************************************
//
//               Demo program for labs
//
// Subject:      Computer Architectures and Parallel systems
// Author:       Petr Olivka, petr.olivka@vsb.cz, 09/2019
// Organization: Department of Computer Science, FEECS,
//               VSB-Technical University of Ostrava, CZ
//
// File:         Main program for I2C bus
//
// **************************************************************************

#include <mbed.h>

#include "i2c-lib.h"
#include "si4735-lib.h"

//************************************************************************

// Direction of I2C communication
#define R	0b00000001
#define W	0b00000000
#define ZERO_BITS 0b00000000
#define ACK_NOT_OK 0

// Serial line for printf output
Serial g_pc(USBTX, USBRX);

DigitalIn g_but9(PTC9);
DigitalIn g_but10(PTC10);
DigitalIn g_but11(PTC11);
DigitalIn g_but12(PTC12);

#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

class PFC8574 {
private:
	uint8_t addr;
	uint8_t ack;
	uint8_t number;

public:
	bool addrOK;
	PFC8574(uint8_t address) :
			addr(address) {
		this->ack = 0;
		this->number = ZERO_BITS;

		i2c_start();

		ack = i2c_output(address);
		addrOK = (ack == 0);

		if (!addrOK)
			g_pc.printf("Wrong address for PFC");

		i2c_stop();
	}
	;

	void set_bit(uint8_t bit, bool val) {

		ack = 0;
		i2c_start();

		ack |= i2c_output(addr);

		//number ^= (-val ^ number) & (1UL << bit);
		//number = (number & ~(1UL << bit)) | (val << bit);
		if (val)
			number |= 1UL << bit;
		else
			number &= 1UL << bit;

		ack |= i2c_output(number);

		if (ack != 0)
			g_pc.printf("Ack ERROR - PFC8574 set_bit \n\r");

		i2c_stop();
	}
	;

	void set_byte(uint8_t byte) {
		ack = 0;
		i2c_start();

		ack |= i2c_output(addr);

		ack |= i2c_output(byte);

		if (ack != 0)
			g_pc.printf("Ack ERROR - PFC8574 set_bit \n\r");

		i2c_stop();
	}
	;

	void reset() {

		ack = 0;
		i2c_start();

		ack |= i2c_output(addr);

		ack |= i2c_output(0);

		if (ack != 0)
			g_pc.printf("Ack ERROR - PFC8574 reset \n\r");

		this->number = ZERO_BITS;

		i2c_stop();
	}

	void bar(int min, int max, int val) {
		this->reset();

		for (int i = min; i <= max && i <= val; i++) {
			this->set_bit(i, 1);
			wait_ms(350);
		}
	}
};

class Si4735 {
private:
	int freq;
	int vol;
	uint8_t S1, S2, RSSI, SNR, MULT, CAP, isStation, STBL, FREQ;
	int vol_step;
	int tune_step;
	char ps[9];
	char rt[65];
	uint8_t addr;
	uint8_t ack;
public:
	Si4735(uint8_t addr) :
			addr(addr) {
		this->vol_step = 5;
		this->tune_step = 0.1 * 100;
		ack = ZERO_BITS;

		i2c_start();

		if ((ack = si4735_init()) != 0) {
			g_pc.printf("Initialization of SI4735 finish with error (%d)\r\n",
					ack);

		} else
			g_pc.printf("SI4735 initialized.\r\n");

		i2c_stop();
	}
	;

	void tune(int freq) {
		ack = ZERO_BITS;
		// Tuning of radio station
		i2c_start();
		ack |= i2c_output( SI4735_ADDRESS | W);
		ack |= i2c_output(0x20);            // FM_TUNE_FREQ
		ack |= i2c_output(0x00);            // ARG1
		ack |= i2c_output(freq >> 8);        // ARG2 - FreqHi
		ack |= i2c_output(freq & 0xff);    // ARG3 - FreqLo
		ack |= i2c_output(0x00);            // ARG4
		i2c_stop();

		if (ack != ACK_NOT_OK) {
			g_pc.printf("ACK Error - tuning.\r\n");
		} else
			g_pc.printf("Sucessfully tuned %d\r\n", freq);

		// Tuning process inside SI4735
		wait_ms(100);

	}
	;

	void tune_up() {
		ack = 0;
		i2c_start();

		ack |= i2c_output(SI4735_ADDRESS | W);
		ack |= i2c_output(0x21);
		ack |= i2c_output(0b00000100);

		if (ack != ACK_NOT_OK) {
			g_pc.printf("ACK Error - seek up.\r\n");
		} else
			g_pc.printf("Sucessfully seek up\r\n");

		i2c_stop();
	}
	;

	void tune_down() {
		ack = 0;
		i2c_start();

		ack |= i2c_output(SI4735_ADDRESS | W);
		ack |= i2c_output(0x21);
		ack |= i2c_output(0b00001100);

		if (ack != ACK_NOT_OK) {
			g_pc.printf("ACK Error - seek down.\r\n");
		} else
			g_pc.printf("Sucessfully seek down\r\n");

		i2c_stop();
	}
	;

	void volume(int VOL) {
		this->vol = VOL;

		ack = 0;
		i2c_start();

		ack |= i2c_output(SI4735_ADDRESS | W);
		ack |= i2c_output(0x12);
		ack |= i2c_output(0x00);
		ack |= i2c_output(0x40);
		ack |= i2c_output(0x00);
		ack |= i2c_output(0x00);
		ack |= i2c_output(VOL);

		if (ack != ACK_NOT_OK) {
			g_pc.printf("ACK Error - volume.\r\n");
		} else
			g_pc.printf("Sucessfully changed volume to %d\r\n", VOL);

		i2c_stop();
	}
	;

	void volume_up() {
		this->volume(this->vol + this->vol_step);
	}
	;

	void volume_down() {
		this->volume(this->vol - this->vol_step);
	}
	;

	int tune_status() {
		ack = 0;
		i2c_start();
		ack |= i2c_output( SI4735_ADDRESS | W);
		ack |= i2c_output(0x22);
		ack |= i2c_output(0x00);

		i2c_start();
		ack |= i2c_output( SI4735_ADDRESS | R);
		S1 = i2c_input();
		i2c_ack();
		S2 = i2c_input();
		i2c_ack();
		freq = i2c_input() << 8;
		i2c_ack();
		freq |= i2c_input();
		i2c_ack();
		RSSI = i2c_input();
		i2c_ack();
		SNR = i2c_input();
		i2c_ack();
		MULT = i2c_input();
		i2c_ack();
		CAP = i2c_input();
		i2c_nack();
		i2c_stop();

		if (ack != ACK_NOT_OK) {
			g_pc.printf("ACK Error - RSSI.\r\n");
			return 0;
		}

		return freq;
	}
	;

	int get_rssi() {
		ack = 0;
		i2c_start();
		ack |= i2c_output( SI4735_ADDRESS | W);
		ack |= i2c_output(0x23);
		ack |= i2c_output(0x00);

		i2c_start();
		ack |= i2c_output( SI4735_ADDRESS | R);
		S1 = i2c_input();
		i2c_ack();
		S2 = i2c_input();
		i2c_ack();
		isStation = i2c_input() & 1;
		i2c_ack();
		STBL = i2c_input();
		i2c_ack();
		RSSI = i2c_input();
		i2c_ack();
		SNR = i2c_input();
		i2c_ack();
		MULT = i2c_input();
		i2c_ack();
		FREQ = i2c_input();
		i2c_nack();
		i2c_stop();

		if (ack != ACK_NOT_OK) {
			g_pc.printf("ACK Error - RSSI.\r\n");
			return 0;
		}

		return RSSI;
	}
	;

	void set_radio_name_to_zeros() {
		for (int i = 0; i < 8; i++) {
			this->ps[i] = 0;
		}

		this->ps[8] = '\0';
	}

	void set_radio_text_to_zeros() {
		for (int i = 0; i < 64; i++) {
			this->rt[i] = 0;
		}

		this->rt[64] = '\0';
	}

	void begin_RDS() {
		ack = 0;
		i2c_start();
		ack |= i2c_output( SI4735_ADDRESS | W);
		ack |= i2c_output(0x24);
		ack |= i2c_output(1);

		if (ack != ACK_NOT_OK) {
			g_pc.printf("ACK Error - begin RDS.\r\n");
		}

		i2c_stop();
	}
	;

	void read_RDS() {
		uint8_t RDS0, RDS1, Sync, RDS3, B1H, B1L, B2H, B2L, B3H, B3L, B4H, B4L,
				last;

		begin_RDS();

		uint8_t ack = 0;
		i2c_start();
		ack |= i2c_output( SI4735_ADDRESS | R);

		RDS0 = i2c_input();
		i2c_ack();

		RDS1 = i2c_input();
		i2c_ack();

		Sync = i2c_input() & 1;
		i2c_ack();

		RDS3 = i2c_input();
		i2c_ack();

		B1H = i2c_input();
		i2c_ack();

		B1L = i2c_input();
		i2c_ack();

		B2H = i2c_input();
		i2c_ack();

		B2L = i2c_input();
		i2c_ack();

		B3H = i2c_input();
		i2c_ack();

		B3L = i2c_input();
		i2c_ack();

		B4H = i2c_input();
		i2c_ack();

		B4L = i2c_input();
		i2c_ack();

		last = i2c_input();
		i2c_nack();
		i2c_stop();

		int blockA_err = last & 0b11000000;
		int blockB_err = last & 0b00110000;
		int blockC_err = last & 0b00001100;
		int blockD_err = last & 0b00000011;

		if(blockB_err == 3) g_pc.printf("Block B is not valid\n\r");

		if (Sync == 1){
            if (B2H >> 4 == 0) {
                int pos = B2L & 0b00000011;
                this->ps[pos] = B4H;
                this->ps[pos + 1] = B4L;
            }

            if (B2H >> 4 == 2) {
                int pos = B2L & 0b00001111;

                this->rt[pos] = B3H;
                this->rt[pos + 1] = B3L;
                this->rt[pos + 2] = B4H;
                this->rt[pos + 3] = B4L;
            }
        }

	}
	;

	void get_program_service(char *text) {
		//this->read_RDS();

		for (int i = 0; i < 9; i++) {
			text[i] = this->ps[i];
		}
	}
	;

	void get_radio_text(char *text) {
		//this->read_RDS();

		for (int i = 0; i < 64; i++) {
			text[i] = this->rt[i];
		}
	}
	;

};

int main(void) {
	i2c_init();

	uint8_t HWADR_PCF8574 = 0b01000000;
	uint8_t A012 = 0b00001000;

	g_pc.baud(115200);
	g_pc.printf("K64F-KIT ready...\r\n");

	PFC8574 pfc(HWADR_PCF8574 | A012 | W);

	if (pfc.addrOK)
		g_pc.printf("PFC8574 addres OK...\r\n");

	/*
	 wait_ms(1000);
	 pfc.reset();
	 wait_ms(1000);
	 pfc.set_bit(5, 1);
	 wait_ms(1000);
	 */
	pfc.bar(10, 80, 50);
	//pfc.set_byte(0b11001111);

	g_pc.printf("\nTunig on radio station...\r\n");

	Si4735 radio(SI4735_ADDRESS);

	// Required frequency in MHz * 100
	//int freq = 10140; // Radiozurnal
	int freq = 9370;

	radio.tune(freq);
	radio.volume(20);

	while (1) {
		if (!g_but9) {
			radio.tune_up();
			g_pc.printf("Actual freq %d\n", radio.tune_status());
		}
		if (!g_but10) {
			radio.tune_down();
			g_pc.printf("Actual freq %d\n", radio.tune_status());
		}
		if (!g_but11)
			radio.volume_up();
		if (!g_but12)
			radio.volume_down();
		wait_ms(50);
	}

	/*
	 char *station = new char[9];
	 char *text = new char[64];

	 //radio.begin_RDS();
	 radio.set_radio_name_to_zeros();
	 radio.set_radio_text_to_zeros();

	 uint8_t RSSI = 0;

	 while(1) {
	 radio.read_RDS();
	 RSSI = radio.get_rssi();
	 radio.get_program_service(station);
	 radio.get_radio_text(text);
	 wait_ms(500);
	 g_pc.printf("Radio: %s\t text: %s\tRSSI %d\r\n", station, text, RSSI);
	 }

	 */

	return 0;

}

