// **************************************************************************
//
//               Demo program for labs
//
// Subject:      Computer Architectures and Parallel systems
// Author:       Petr Olivka, petr.olivka@vsb.cz, 08/2016
// Organization: Department of Computer Science, FEECS,
//               VSB-Technical University of Ostrava, CZ
//
// File:         Main programm for I2C bus
//
// **************************************************************************

#include <mbed.h>

#include "i2c-lib.h"
#include "si4735-lib.h"



//************************************************************************

// Direction of I2C communication
#define R    0b00000001
#define W    0b00000000
#define ZERO_BITS 0b00000000
#define ACK_NOT_OK 0

Serial pc( USBTX, USBRX );

#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

class PFC8574 {
private:
    uint8_t addr;
    uint8_t ack;
    uint8_t number;
    void test_led(){
        for(uint8_t i = 0; i < 8; i++)
        {
            this->set_bit(i, 1);
            wait_ms(500);
        }
    };
public:
    bool addrOK;
    PFC8574(uint8_t address): addr(address) {
        this->ack = 0;
        this->number = ZERO_BITS;

        I2C_Start();

        ack = I2C_Output( address );
        addrOK = (ack != 0);

        this->test_led();

        I2C_Stop();

    };

    void set_bit(uint8_t bit, bool val){

        ack = 0;
        I2C_Start();

        ack |= I2C_Output( addr );

        number ^= (-val ^ number) & (1UL << bit);

        ack |= I2C_Output(number);

        if(ack != 0) pc.printf("Ack ERROR - PFC8574 set_bit \n\r");

        I2C_Stop();
    };

    void reset(){

        ack = 0;
        I2C_Start();

        ack |= I2C_Output( addr );

        ack |= I2C_Output(0);

        if(ack != 0) pc.printf("Ack ERROR - PFC8574 reset \n\r");

        this->number = ZERO_BITS;

        I2C_Stop();
    }
};

class Si4735 {
private:
    int freq;
    int vol;
    uint8_t S1, S2, RSSI, SNR, MULT, CAP, isStation, STBL, FREQ;
    int vol_step;
    int tune_step;
    char ps[9];
    char rt[65];
    uint8_t addr;
    uint8_t ack;
public:
    Si4735(uint8_t addr): addr(addr){
        this->vol_step = 5;
        this->tune_step = 0.1 * 100;
        ack = ZERO_BITS;

        I2C_Start();

        if ( ( ack = SI4735_Init() ) != 0 )
        {
            pc.printf( "Initialization of SI4735 finish with error (%d)\r\n", ack );

        }
        else
            pc.printf( "SI4735 initialized.\r\n" );

        I2C_Stop();
    };

    void tune(int freq){
        ack = ZERO_BITS;
        // Tuning of radio station
        I2C_Start();
        ack |= I2C_Output( SI4735_address | W );
        ack |= I2C_Output( 0x20 );            // FM_TUNE_FREQ
        ack |= I2C_Output( 0x00 );            // ARG1
        ack |= I2C_Output( freq >> 8 );        // ARG2 - FreqHi
        ack |= I2C_Output( freq & 0xff );    // ARG3 - FreqLo
        ack |= I2C_Output( 0x00 );            // ARG4
        I2C_Stop();

        if ( ack != ACK_NOT_OK )
        {
            pc.printf( "ACK Error - tuning.\r\n" );
        }
        else
            pc.printf( "Sucessfully tuned %d\r\n", freq);


        // Tuning process inside SI4735
        wait_ms( 100 );

    };

    void up() {
        ack = 0;
        I2C_Start();

        ack |= I2C_Output(SI4735_address | W);
        ack |= I2C_Output(0x21);
        ack |= I2C_Output(0b00000100);

        if ( ack != ACK_NOT_OK )
        {
            pc.printf( "ACK Error - seek up.\r\n" );
        }
        else
            pc.printf( "Sucessfully seek up\r\n");

        I2C_Stop();
    };

    void down() {
        ack = 0;
        I2C_Start();

        ack |= I2C_Output(SI4735_address | W);
        ack |= I2C_Output(0x21);
        ack |= I2C_Output(0b00001100);

        if ( ack != ACK_NOT_OK )
        {
            pc.printf( "ACK Error - seek down.\r\n" );
        }
        else
            pc.printf( "Sucessfully seek down\r\n");

        I2C_Stop();
    };

    void volume(char VOL) {
        ack = 0;
        I2C_Start();

        ack |= I2C_Output(SI4735_address | W);
        ack |= I2C_Output(0x12);
        ack |= I2C_Output(0x00);
        ack |= I2C_Output(0x40);
        ack |= I2C_Output(0x00);
        ack |= I2C_Output(0x00);
        ack |= I2C_Output(VOL);

        if ( ack != ACK_NOT_OK )
        {
            pc.printf( "ACK Error - volume.\r\n" );
        }
        else
            pc.printf( "Sucessfully changed volume to %d\r\n", VOL);

        I2C_Stop();
    };

    int get_rssi() {
        ack = 0;
        I2C_Start();
        ack |= I2C_Output( SI4735_address | W );
        ack |= I2C_Output( 0x23 );
        ack |= I2C_Output( 0x00 );

        I2C_Start();
        ack |= I2C_Output( SI4735_address | R );
        S1 = I2C_Input();
        I2C_Ack();
        S2 = I2C_Input();
        I2C_Ack();
        isStation = I2C_Input() & 1;
        I2C_Ack();
        STBL = I2C_Input();
        I2C_Ack();
        RSSI = I2C_Input();
        I2C_Ack();
        SNR = I2C_Input();
        I2C_Ack();
        MULT = I2C_Input();
        I2C_Ack();
        FREQ = I2C_Input();
        I2C_NAck();
        I2C_Stop();

        if ( ack != ACK_NOT_OK )
        {
            pc.printf( "ACK Error - RSSI.\r\n" );
            return 0;
        }

        return RSSI;
    };

    void set_radio_name_to_zeros(){
        for(int i = 0; i < 9; i++){
            this->ps[i] = 0;
        }

        this->ps[8] = '\0';
    }

    void set_radio_text_to_zeros(){
        for(int i = 0; i < 65; i++){
            this->rt[i] = 0;
        }

        this->rt[64] = '\0';
    }

    void begin_RDS(){
        ack = 0;
        I2C_Start();
        ack |= I2C_Output( SI4735_address | W );
        ack |= I2C_Output( 0x24 );
        ack |= I2C_Output( 1 );

        if ( ack != ACK_NOT_OK )
        {
            pc.printf( "ACK Error - begin RDS.\r\n" );
        }

        I2C_Stop();
    };


    void read_RDS(){
        uint8_t RDS0, RDS1, Sync, RDS3, B1H, B1L, B2H, B2L, B3H, B3L, B4H, B4L, last;

        begin_RDS();

        uint8_t ack = 0;
        I2C_Start();
        ack |= I2C_Output( SI4735_address | R );

        RDS0 = I2C_Input();
        I2C_Ack();

        RDS1 = I2C_Input();
        I2C_Ack();

        Sync = I2C_Input();
        I2C_Ack();

        RDS3 = I2C_Input();
        I2C_Ack();

        B1H = I2C_Input();
        I2C_Ack();

        B1L = I2C_Input();
        I2C_Ack();

        B2H = I2C_Input();
        I2C_Ack();

        B2L = I2C_Input();
        I2C_Ack();

        B3H = I2C_Input();
        I2C_Ack();

        B3L = I2C_Input();
        I2C_Ack();

        B4H = I2C_Input();
        I2C_Ack();

        B4L = I2C_Input();
        I2C_Ack();

        last = I2C_Input();
        I2C_NAck();
        I2C_Stop();


        if(B2H >> 4 == 0) {
            int pos1 = B2L & 0b00000010;
            int pos2 = B2L & 0b00000001;

            switch( pos1 | pos2 ) {
                case 0b00000000:
                    this->ps[0] = B4H;
                    this->ps[1] = B4L;
                    break;
                case 0b00000001:
                    this->ps[2] = B4H;
                    this->ps[3] = B4L;
                    break;
                case 0b00000010:
                    this->ps[4] = B4H;
                    this->ps[5] = B4L;
                    break;
                case 0b00000011:
                    this->ps[6] = B4H;
                    this->ps[7] = B4L;
                    break;
            }
        }

        if(B2H >> 4 == 2) {
            int pos1 = B2L & 0b00001000;
            int pos2 = B2L & 0b00000100;
            int pos3 = B2L & 0b00000010;
            int pos4 = B2L & 0b00000001;

            int pos = pos1 | pos2 | pos3 | pos4;

            switch(pos){
                case 0:
                    this->rt[0] = B3H;
                    this->rt[1] = B3L;
                    this->rt[2] = B4H;
                    this->rt[3] = B4L;
                    break;
                case 1:
                    this->rt[4] = B3H;
                    this->rt[5] = B3L;
                    this->rt[6] = B4H;
                    this->rt[7] = B4L;
                    break;
                case 2:
                    this->rt[8] = B3H;
                    this->rt[9] = B3L;
                    this->rt[10] = B4H;
                    this->rt[11] = B4L;
                    break;
                case 3:
                    this->rt[12] = B3H;
                    this->rt[13] = B3L;
                    this->rt[14] = B4H;
                    this->rt[15] = B4L;
                    break;
                case 4:
                    this->rt[16] = B3H;
                    this->rt[17] = B3L;
                    this->rt[18] = B4H;
                    this->rt[19] = B4L;
                    break;
                case 5:
                    this->rt[20] = B3H;
                    this->rt[21] = B3L;
                    this->rt[22] = B4H;
                    this->rt[23] = B4L;
                    break;
                case 6:
                    this->rt[24] = B3H;
                    this->rt[25] = B3L;
                    this->rt[26] = B4H;
                    this->rt[27] = B4L;
                    break;
                case 7:
                    this->rt[28] = B3H;
                    this->rt[29] = B3L;
                    this->rt[30] = B4H;
                    this->rt[31] = B4L;
                    break;
                case 8:
                    this->rt[32] = B3H;
                    this->rt[33] = B3L;
                    this->rt[34] = B4H;
                    this->rt[35] = B4L;
                    break;
                case 9:
                    this->rt[36] = B3H;
                    this->rt[37] = B3L;
                    this->rt[38] = B4H;
                    this->rt[39] = B4L;
                    break;
                case 10:
                    this->rt[40] = B3H;
                    this->rt[41] = B3L;
                    this->rt[42] = B4H;
                    this->rt[43] = B4L;
                    break;
                case 11:
                    this->rt[44] = B3H;
                    this->rt[45] = B3L;
                    this->rt[46] = B4H;
                    this->rt[47] = B4L;
                    break;
                case 12:
                    this->rt[48] = B3H;
                    this->rt[49] = B3L;
                    this->rt[50] = B4H;
                    this->rt[51] = B4L;
                    break;
                case 13:
                    this->rt[52] = B3H;
                    this->rt[53] = B3L;
                    this->rt[54] = B4H;
                    this->rt[55] = B4L;
                    break;
                case 14:
                    this->rt[56] = B3H;
                    this->rt[57] = B3L;
                    this->rt[58] = B4H;
                    this->rt[59] = B4L;
                    break;
                case 15:
                    this->rt[60] = B3H;
                    this->rt[61] = B3L;
                    this->rt[62] = B4H;
                    this->rt[63] = B4L;
                    break;
            }

        }
    };


    void get_program_service(char *text){
        //this->read_RDS();

        for(int i = 0; i < 9; i++)
        {
            text[i] = this->ps[i];
        }
    };

    void get_radio_text(char *text){
        //this->read_RDS();

        for(int i = 0; i < 64; i++)
        {
            text[i] = this->rt[i];
        }
    };

};


int main( void )
{
    I2C_Init();

    uint8_t HWADR_PCF8574 = 0b01000000;
    uint8_t A012 = 0b00001110;

    pc.baud( 115200 );
    pc.printf( "K64F-KIT ready...\r\n" );


    PFC8574 pfc(HWADR_PCF8574 | A012 | W);

    if(pfc.addrOK) pc.printf( "PFC8574 addres OK...\r\n" );

    wait_ms(1000);
    pfc.reset();
    wait_ms(1000);
    pfc.set_bit(5, 1);


    pc.printf( "\nTunig on radio station...\r\n" );


    Si4735 radio(SI4735_address);

    // Required frequency in MHz * 100
    //int freq = 10140; // Radiozurnal
    int freq = 8900;

    radio.tune(freq);

    char *station = new char[9];
    char *text = new char[64];

    //radio.begin_RDS();
    radio.set_radio_name_to_zeros();
    radio.set_radio_text_to_zeros();

    uint8_t RSSI = 0;

    while(1) {
        radio.read_RDS();
        RSSI = radio.get_rssi();
        radio.get_program_service(station);
        radio.get_radio_text(text);
        wait_ms(500);
        pc.printf("Radio: %s\t text: %s\tRSSI %d\r\n", station, text, RSSI);
    }



    return 0;
}
