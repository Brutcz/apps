// **************************************************************************
//
//               Demo program for labs
//
// Subject:      Computer Architectures and Parallel systems
// Author:       Petr Olivka, petr.olivka@vsb.cz, 09/2019
// Organization: Department of Computer Science, FEECS,
//               VSB-Technical University of Ostrava, CZ
//
// File:         OpenCV simulator of LCD
//
// **************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <opencv2/opencv.hpp>
#include "font8x8.h"
#include <unistd.h>
#include "fonts/font10x16_lsb.h"

#define LCD_WIDTH 320
#define LCD_HEIGHT 240

#define FONT_WIDTH 10
#define FONT_HEIGHT 16
#define FONT font
#define LCD_NAME        "Virtual LCD"

// LCD Simulator

// Virtual LCD
cv::Mat g_canvas( cv::Size( LCD_WIDTH, LCD_HEIGHT ), CV_8UC3 );

// Put color pixel on LCD (canvas)
void lcd_put_pixel( int t_x, int t_y, int t_rgb_565 )
{
    // Transform the color from a LCD form into the OpenCV form. 
    cv::Vec3b l_rgb_888( 
            (  t_rgb_565         & 0x1F ) << 3, 
            (( t_rgb_565 >> 5 )  & 0x3F ) << 2, 
            (( t_rgb_565 >> 11 ) & 0x1F ) << 3
            );
    g_canvas.at<cv::Vec3b>( t_y, t_x ) = l_rgb_888; // put pixel
}

// Clear LCD
void lcd_clear()
{
    cv::Vec3b l_black( 0, 0, 0 );
    g_canvas.setTo( l_black );
}

// LCD Initialization 
void lcd_init()
{
    cv::namedWindow( LCD_NAME, 0 );
    lcd_clear();
    cv::waitKey( 1 );
}

void wait_ms(int ms)
{
    usleep(ms * 100);
}

// Simple graphic interface

struct Point2D
{
    int32_t x, y;
};

struct RGB
{
    uint8_t r, g, b;
};

struct Time
{
    int h, m , s;
};

class GraphElement
{
public:
    // foreground and background color
    RGB fg_color, bg_color;

    // constructor
    GraphElement(RGB t_fg_color, RGB t_bg_color) : fg_color(t_fg_color), bg_color(t_bg_color)
    {
    }

    // ONLY ONE INTERFACE WITH LCD HARDWARE!!!
    void drawPixel(int32_t t_x, int32_t t_y)
    {
        lcd_put_pixel(t_x, t_y, convert_RGB888_to_RGB565(fg_color));
    }

    // Draw graphics element
    virtual void draw() = 0;

    // Hide graphics element
    virtual void hide()
    {
        swap_fg_bg_color();
        draw();
        swap_fg_bg_color();
    }

private:
    // swap foreground and backgroud colors
    void swap_fg_bg_color()
    {
        RGB l_tmp = fg_color;
        fg_color = bg_color;
        bg_color = l_tmp;
    }

    // conversion of 24-bit RGB color into 16-bit color format
    int convert_RGB888_to_RGB565(RGB t_color)
    {
        int B = (t_color.b >> 3) & 0x001F;
        int G = ((t_color.g >> 2) << 5) & 0x07E0;  // not <
        int R = ((t_color.r >> 3) << 11) & 0xF800; // not <
        return (int)(R | G | B);
    }
};

class Pixel : public GraphElement
{
public:
    // constructor
    Pixel(Point2D t_pos, RGB t_fg_color, RGB t_bg_color) : pos(t_pos), GraphElement(t_fg_color, t_bg_color)
    {
    }
    // Draw method implementation
    virtual void draw()
    {
        drawPixel(pos.x, pos.y);
    }
    // Position of Pixel
    Point2D pos;
};

class Circle : public GraphElement
{
private:
    void drawCircle(int xc, int yc, int x, int y)
    {
        drawPixel(xc + x, yc + y);
        drawPixel(xc - x, yc + y);
        drawPixel(xc + x, yc - y);
        drawPixel(xc - x, yc - y);
        drawPixel(xc + y, yc + x);
        drawPixel(xc - y, yc + x);
        drawPixel(xc + y, yc - x);
        drawPixel(xc - y, yc - x);
    };

public:
    // Center of circle
    Point2D center;
    // Radius of circle
    int32_t radius;

    Circle(Point2D t_center, int32_t t_radius, RGB t_fg, RGB t_bg) : center(t_center), radius(t_radius), GraphElement(t_fg, t_bg){};

    void draw()
    {
        int x = 0, y = this->radius;
        int d = 3 - 2 * this->radius;
        this->drawCircle(this->center.x, this->center.y, x, y);
        while (y >= x)
        {
            x++;

            if (d > 0)
            {
                y--;
                d = d + 4 * (x - y) + 10;
            }
            else
                d = d + 4 * x + 6;
            this->drawCircle(this->center.x, this->center.y, x, y);
        }
    }
};

class Character : public GraphElement
{
public:

// position of character
    Point2D pos;
    // character
    char character;

    Character(Point2D t_pos, char t_char, RGB t_fg, RGB t_bg) : pos(t_pos), character(t_char), GraphElement(t_fg, t_bg){};
    Character() : pos(Point2D{0, 0}), character((char)0), GraphElement(RGB{255, 255, 255}, RGB{0, 0, 0}) {};

    void draw()
    {
        for (int y = 0; y < FONT_HEIGHT; y++)
        {
            int radek_fontu = FONT[(int)this->character][y];
            for (int x = 0; x < FONT_WIDTH; x++)
            {
                if (radek_fontu & (1 << x))
                    drawPixel(this->pos.x + x, this->pos.y + y);
            }
        }
    };
};

class Text : public GraphElement
{
private:
    int len;
    Character * chars;
public:
    Point2D pos;

    Text(const char * str, Point2D t_pos, RGB t_fg, RGB t_bg) : pos(t_pos), GraphElement(t_fg, t_bg)
    {
        len = strlen(str);
        chars = new Character[len];
        for (int i = 0; i < len; ++i) {
            chars[i] = Character(Point2D{pos.x + i*FONT_WIDTH, pos.y}, str[i], fg_color, bg_color);
        }
    };

    void draw()
    {
        for (int i = 0; i < len; ++i) {
            chars[i].draw();
        }
    };

    void hide()
    {
        for (int i = 0; i < len; ++i) {
            chars[i].hide();
        }
    };

    void moveXByStep(int step)
    {
        this->pos.x += step;
        this->hide();
        for (int i = 0; i < len; ++i) {
            chars[i].pos.x += step;
        }
        this->draw();
    }
};

class Line : public GraphElement
{
public:
    // the first and the last point of line
    Point2D pos1, pos2;

    Line(Point2D t_pos1, Point2D t_pos2, RGB t_fg, RGB t_bg) : pos1(t_pos1), pos2(t_pos2), GraphElement(t_fg, t_bg){};
    Line() : pos1(Point2D{0, 0}), pos2(Point2D{0, 0}), GraphElement(RGB{255, 255, 255}, RGB{0, 0, 0}){};

    void draw(){
        int x0 = pos1.x;
        int x1 = pos2.x;
        int y0 = pos1.y;
        int y1 = pos2.y;

        int dx = abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
        int dy = -abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
        int err = dx + dy, e2; /* error value e_xy */

        while (1)
        {
            drawPixel(x0, y0);
            if (x0 == x1 && y0 == y1)
                break;
            e2 = 2 * err;
            if (e2 >= dy)
            {
                err += dy;
                x0 += sx;
            } /* e_xy+e_x > 0 */
            if (e2 <= dx)
            {
                err += dx;
                y0 += sy;
            } /* e_xy+e_y < 0 */
        }
    };
};

struct ClockArrows
{
    Line *h, *m, *s;
};

class Clock : public GraphElement
{
public:
    // Center of clock
    Point2D center;
    // Radius of clock
    int32_t radius;

    Time time = {10, 58, 45};
    ClockArrows arrows = {nullptr, nullptr, nullptr};

    Clock(Point2D t_center, int32_t t_radius, RGB t_fg, RGB t_bg) : center(t_center), radius(t_radius), GraphElement(t_fg, t_bg){};

    void draw()
    {
        this->drawBody();
        this->drawFace();
        this->drawArrows();
    };

    void tick()
    {
        this->time.s += 1;
        if(this->time.s >= 60) { this->time.m++; this->time.s = 0; }
        if(this->time.m >= 60) { this->time.h++; this->time.m = 0; }
        if(this->time.h > 12) { this->time.h = 1; }

        // printf("Time is %d:%d:%d\n", this->time.h, this->time.m, this->time.s);

        this->drawArrows();
    }
private:
    void drawBody()
    {
        Circle body(center, radius, fg_color, bg_color);
        body.draw();
    };

    void drawFace()
    {
        for (int i = 0; i < 12; i++) {
            double x_cos = cos(-(M_PI/2) - (i + 1) * -(M_PI / 6));
            double y_sin = sin(-(M_PI/2) - (i + 1) * -(M_PI / 6));
            double ratio = .9;
            Point2D point = {center.x + (int)(x_cos * radius * ratio), center.y + (int)(y_sin * radius * ratio)};
            if(i >= 10) point.x -= FONT_WIDTH/2;
            Text t(std::to_string(i + 1).c_str(), point, fg_color, bg_color);
            t.draw();
        }
    };

    void drawArrows()
    {
        Point2D h_pos = {center.x + (int)(cos(-(M_PI/2) - this->time.h * -(M_PI / 6)) * radius * .5), center.y + (int)(sin(-(M_PI/2) - this->time.h * -(M_PI / 6)) * radius * .5)};
        Point2D m_pos = {center.x + (int)(cos(-(M_PI/2) - this->time.m * -(M_PI / 30)) * radius * .7), center.y + (int)(sin(-(M_PI/2) - this->time.m * -(M_PI / 30)) * radius * .7)};
        Point2D s_pos = {center.x + (int)(cos(-(M_PI/2) - this->time.s * -(M_PI / 30)) * radius * .8), center.y + (int)(sin(-(M_PI/2) - this->time.s * -(M_PI / 30)) * radius * .8)};

        if(this->arrows.h == nullptr)
        {
            this->arrows.h = new Line(center, h_pos, fg_color, bg_color);
        } else {
            this->arrows.h->hide();
            this->arrows.h->pos2 = h_pos;
        }
        this->arrows.h->draw();

        if(this->arrows.m == nullptr)
        {
            this->arrows.m = new Line(center, m_pos, fg_color, bg_color);
        } else {
            this->arrows.m->hide();
            this->arrows.m->pos2 = m_pos;
        }
        this->arrows.m->draw();

        if(this->arrows.s == nullptr)
        {
            this->arrows.s = new Line(center, s_pos, fg_color, bg_color);
        } else {
            this->arrows.s->hide();
            this->arrows.s->pos2 = s_pos;
        }
        this->arrows.s->draw();
    };
};


class Cart : public GraphElement
{
private:
	int bodyHeigh = 10;
public:
	Point2D pos;
	Line *body;
	Circle *wheels;

	Cart(Point2D t_pos, int width, RGB t_fg, RGB t_bg) : pos(t_pos), GraphElement(t_fg, t_bg)
	{
		body = new Line[4];
		wheels = new Circle[2];

		body[0] = Line(pos, Point2D{ pos.x + width, pos.y}, fg_color, bg_color);
		body[1] = Line(Point2D{ pos.x + width, pos.y}, Point2D{ pos.x + width, pos.y + bodyHeigh}, fg_color, bg_color);
		body[2] = Line(Point2D{ pos.x, pos.y + bodyHeigh}, Point2D{ pos.x + width, pos.y + bodyHeigh}, fg_color, bg_color);
		body[3] = Line(pos, Point2D{ pos.x, pos.y + bodyHeigh}, fg_color, bg_color);

		wheels[0] = Circle(Point2D{pos.x + (width / 4), pos.y + (bodyHeigh * 2)}, bodyHeigh,fg_color, bg_color);
		wheels[1] = Circle(Point2D{pos.x + width - (width / 4), pos.y + (bodyHeigh * 2)}, bodyHeigh,fg_color, bg_color);
	};

	void draw()
	{
		drawBody();
		drawWheels();
	};

	void drawBody()
	{
		for(int i = 0; i < 4; i++)
			body[i].draw();
	};

	void drawWheels()
	{
		for(int i = 0; i < 2; i++)
			wheels[i].draw();
	};

	void hide()
	{
		for(int i = 0; i < 4; i++)
			body[i].hide();

		for(int i = 0; i < 2; i++)
			wheels[i].hide();
	};

	void moveXByStep(int step)
	{
		this->hide();
		for(int i = 0; i < 4; i++)
		{
			body[i].pos1.x += step;
			body[i].pos2.x += step;
		}

		for(int i = 0; i < 2; i++)
			wheels[i].center.x += step;

		this->draw();
	};

	void setColor(RGB color)
	{
		this->hide();
		for(int i = 0; i < 4; i++)
			body[i].fg_color = color ;

		for(int i = 0; i < 2; i++)
			wheels[i].fg_color = color;

		this->draw();
	};
};


int random(int min, int max) //range : [min, max)
{
    static bool first = true;
    if (first)
    {
        srand(time(NULL)); //seeding for the first time only!
        first = false;
    }
    return min + rand() % ((max + 1) - min);
}

RGB color_red = {255, 0, 0};
RGB color_yellow = {255, 255, 0};
RGB color_blue = {0, 0, 255};
RGB color_green = {0, 255, 0};
RGB color_black = {0, 0, 0};
RGB color_white = {255, 255, 255};
RGB color_purple = {255, 0, 255};
RGB color_aqua = {0, 255, 255};

RGB colors[8] = {color_red, color_yellow, color_blue, color_green, color_black, color_white, color_purple, color_aqua};

Point2D dCenter = {LCD_WIDTH/2, LCD_HEIGHT/2};

int main()
{
    lcd_init();                     // LCD initialization

    lcd_clear();                    // LCD clear screen

    Point2D start = {50, 100};

    // Character test(start, (char)65, color_white, color_black);
    // test.draw();

//     Point2D s = {random(0, LCD_WIDTH - FONT_WIDTH), random(0, LCD_HEIGHT - FONT_HEIGHT)};
//     Character ch(s, (char)random(65, 126), colors[random(0, 8)], colors[random(0, 8)]);
//     ch.draw();
    
    /*
    while (1)
    {
        Point2D start = {random(0, LCD_WIDTH - FONT_WIDTH), random(0, LCD_HEIGHT - FONT_HEIGHT)};
        Character ch(start, (char)random(23, 126), colors[random(0, 8)], colors[random(0, 8)]);
        ch.draw();
        cv::imshow( LCD_NAME, g_canvas );   // refresh content of "LCD"
        cv::waitKey( 500 );    
        // wait_ms(500);
    }*/

//    Circle c(dCenter, 100, color_white, color_black);
//    c.draw();
//
//    Line l(start, dCenter, color_white, color_black);
//    l.draw();

//    Clock clock(dCenter, 115, color_white, color_black);
//    clock.draw();
//
//    while(1)
//    {
//        clock.tick();
//        cv::imshow( LCD_NAME, g_canvas );   // refresh content of "LCD"
//        cv::waitKey( 1000 );
//    }

    int step = 5;
    bool r2l = true;

    const char * login_text = "BYR0046";
    int login_len = strlen(login_text);
    Point2D login_pos = {dCenter.x - (int)((login_len / 2.0) * FONT_WIDTH), dCenter.y - FONT_HEIGHT};

    Text login(login_text, login_pos, color_white, color_black);
    login.draw();

    while(1)
    {
        login.moveXByStep((int)((r2l)? step : -step));
        if( (login.pos.x + FONT_WIDTH * (login_len + 1)) > LCD_WIDTH || 0 > (login.pos.x + FONT_WIDTH * (login_len + 1)) ) r2l = !r2l;
        cv::imshow( LCD_NAME, g_canvas );   // refresh content of "LCD"
        cv::waitKey( 100 );
    }
    
    /*
    int step = 5;
	bool r2l = true;
	int color_index = 0;


	const char * login_text = "BYR0046";
	int login_len = strlen(login_text);
	Point2D login_pos = { dCenter.x - (int) ((login_len / 2.0) * FONT_WIDTH), dCenter.y - (int)(FONT_HEIGHT / 2.0) };

	Text login(login_text, login_pos, color_white, color_black);
	login.draw();

	Cart cart(Point2D{login_pos.x , login_pos.y + FONT_HEIGHT + 1}, login_len * FONT_WIDTH, color_white, color_black);
	cart.draw();

	while (1) {
		int moveBy = (r2l) ? step : -step;
		login.moveXByStep(moveBy);
		cart.moveXByStep(moveBy);
		if (
			(login.pos.x + (FONT_WIDTH * (login_len + 1))) > LCD_WIDTH
				||
			0 > (login.pos.x + FONT_WIDTH)
		   )
			r2l = !r2l;
		if(!g_but9)
		{
			//color_index = (!g_but10 + !g_but11 + !g_but12);
			RGB barva = { (!g_but10)? 255 : 0, (!g_but11)? 255 : 0, (!g_but12)? 255 : 0 };
			login.setColor(barva);
			cart.setColor(barva);
		}
		wait_ms(50);
	}
    */

//    cv::imshow( LCD_NAME, g_canvas );   // refresh content of "LCD"
//    cv::waitKey( 0 );                   // wait for key

    return 0;
}